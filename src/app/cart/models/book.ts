


  export class Book {
  id: number;
  title:  string;
  price:  number  ;
  stock: number;
  coverPage: string;
  quantity:number;

}
