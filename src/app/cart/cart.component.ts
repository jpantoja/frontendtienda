import { Component, OnInit } from '@angular/core';
import { Cart } from './cart';
import { CartService } from './cart.service';
//import { ModalService } from './detalle/modal.service';
import swal from 'sweetalert2';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../usuarios/auth.service';

import { URL_BACKEND } from '../config/config';
import { SharedService } from '../Services/shared.service';

import { BooksService } from '../books/books.service';
import { Book } from './models/book';
import { ShoppingCart } from './models/shoppingCart';
import { ItemsShoppingCart } from './models/itemsShoppingCart';
import { Router } from '@angular/router';
import { User } from './models/user';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html'
})
export class CartComponent implements OnInit {

  total: number;



  shoppingCart: ShoppingCart = new ShoppingCart();
  //booksAddedTocart: Book[];

  constructor(private cartService: CartService,
    private booksService: BooksService,
    private sharedService: SharedService,
    public authService: AuthService,
    private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.shoppingCart = this.booksService.getbookFromCart();

    /*
        for (let i in this.shoppingCart.itemsShoppingCart) {
          this.shoppingCart.itemsShoppingCart[i].quantity= this.shoppingCart.itemsShoppingCart[i].book.quantity    ;
       }*/
    if (this.shoppingCart !== null) {
      if (this.shoppingCart.itemsShoppingCart !== null) {
        if (this.shoppingCart.itemsShoppingCart.length > 0) {
          this.sharedService.updateCartCount(this.shoppingCart.itemsShoppingCart.length);
          this.calcularTotal(this.shoppingCart.itemsShoppingCart);
        }
      }
    }




  }


  eliminarBook(id: number) {


    this.shoppingCart.itemsShoppingCart = this.shoppingCart.itemsShoppingCart.filter((itemsShoppingCart: ItemsShoppingCart) => id !== itemsShoppingCart.book.id);

    this.booksService.removeAllbookFromCart();
    this.booksService.addbookToCart(this.shoppingCart);
    this.calcularTotal(this.shoppingCart.itemsShoppingCart);

    this.sharedService.updateCartCount(this.shoppingCart.itemsShoppingCart.length);

  }

  actualizarCantidad(id: number, event: any, itemsShoppingCart2: ItemsShoppingCart): void {
    let cant: number = event.target.value as number;


    this.shoppingCart.itemsShoppingCart = this.shoppingCart.itemsShoppingCart.map((itemsShoppingCart: ItemsShoppingCart) => {
      if (id === itemsShoppingCart.book.id) {


        if (cant > itemsShoppingCart.book.stock) {
          swal.fire('Advertencia: ', `Solo hay disponible  ${itemsShoppingCart.book.stock} unidades.`, 'warning');
          //  itemsShoppingCart.quantity = itemsShoppingCart.book.stock;
          // this.calcularTotal(this.shoppingCart.itemsShoppingCart);



          itemsShoppingCart2.quantity = itemsShoppingCart.book.stock;
          this.shoppingCart.itemsShoppingCart.filter((itemsShoppingCart: ItemsShoppingCart) => id !== itemsShoppingCart.book.id)
          this.booksService.removeAllbookFromCart();
          this.booksService.addbookToCart(this.shoppingCart);
          this.shoppingCart.itemsShoppingCart.push(itemsShoppingCart2);
          this.calcularTotal(this.shoppingCart.itemsShoppingCart);
          this.sharedService.updateCartCount(this.shoppingCart.itemsShoppingCart.length);


        } if (cant <= 0 || cant === undefined) {

          swal.fire('Advertencia: ', 'Debe seleccionar minimo una unidad o eliminar el libro del carrito', 'warning');
          // itemsShoppingCart.quantity = 1;
          //  this.calcularTotal(this.shoppingCart.itemsShoppingCart);

          itemsShoppingCart2.quantity = 1;
          this.shoppingCart.itemsShoppingCart.filter((itemsShoppingCart: ItemsShoppingCart) => id !== itemsShoppingCart.book.id)
          this.booksService.removeAllbookFromCart();
          this.booksService.addbookToCart(this.shoppingCart);
          this.shoppingCart.itemsShoppingCart.push(itemsShoppingCart2);
          this.calcularTotal(this.shoppingCart.itemsShoppingCart);
          this.sharedService.updateCartCount(this.shoppingCart.itemsShoppingCart.length);

        } else

          if
            (Number(cant) <= Number(itemsShoppingCart.book.stock) && Number(cant) > 0) {

            //  itemsShoppingCart.quantity = cant;

            itemsShoppingCart2.quantity = itemsShoppingCart.book.stock;
            this.shoppingCart.itemsShoppingCart.filter((itemsShoppingCart: ItemsShoppingCart) => id !== itemsShoppingCart.book.id)
            this.booksService.removeAllbookFromCart();
            this.booksService.addbookToCart(this.shoppingCart);
            this.shoppingCart.itemsShoppingCart.push(itemsShoppingCart2);
            this.calcularTotal(this.shoppingCart.itemsShoppingCart);
            this.sharedService.updateCartCount(this.shoppingCart.itemsShoppingCart.length);
            //  this.calcularTotal(this.shoppingCart.itemsShoppingCart);
          }

      }

      return itemsShoppingCart;
    });
  }


  confirmarCompra(): void {

    swal.fire({
      title: 'Confirmar Compra.',
      text: "Desea confirmar y terminar su compra",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si ',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {

        let nuevoItem = new User();
        this.shoppingCart = this.booksService.getbookFromCart();
        nuevoItem.username = this.authService.usuario.username;
        this.shoppingCart.user = nuevoItem;
        let obj = <ShoppingCart>this.shoppingCart;
        console.log(JSON.stringify(obj));

        this.cartService.create(obj).subscribe(shoppingCart => {
          swal.fire(
            'Información!',
            'Hemos recibido su solicitud y será procesada por nuestros agentes. Gracias por su compra',
            'success'
          )
          this.booksService.removeAllbookFromCart();


          this.shoppingCart = new ShoppingCart();
          this.sharedService.updateCartCount(0);


          this.router.navigate(['/books']);
        });



      }
    })
  }

  cancelarCompra(): void {
    swal.fire({
      title: 'Estas Seguro?',
      text: "Deseas Cancelar La compra?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si ',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.booksService.removeAllbookFromCart();
        this.shoppingCart = new ShoppingCart();
        this.sharedService.updateCartCount(0);

        swal.fire(
          'Información!',
          'Su compra se ha cancelado',
          'success'
        )

      }
    })
  }










  calcularSubTotal(itemsShoppingCart: ItemsShoppingCart): number {

    return (itemsShoppingCart.quantity * itemsShoppingCart.book.price);


  }


  calcularTotal(allItems: ItemsShoppingCart[]) {
    let total = 0;
    for (let i in allItems) {
      total = total + (allItems[i].quantity * allItems[i].book.price);
    }
    this.total = total;
  }

}















