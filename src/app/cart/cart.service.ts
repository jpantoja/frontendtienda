import { Injectable } from '@angular/core';
import { Cart } from './cart';

import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Router } from '@angular/router';

import { URL_BACKEND } from '../config/config';
import { ShoppingCart } from './models/shoppingCart';
@Injectable()
export class CartService {
  private urlEndPoint: string = URL_BACKEND + '/api/shoppingCart';

  constructor(private http: HttpClient, private router: Router) { }

  removeAllbookFromCart() {
    return localStorage.removeItem("shoppingCart");
  }


  create(shoppingCart: ShoppingCart): Observable<ShoppingCart> {
    return this.http.post<ShoppingCart>(this.urlEndPoint, shoppingCart);
  }


}
