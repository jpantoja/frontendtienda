import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';





import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatDatepickerModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { LoginComponent } from './usuarios/login.component';
import { AuthGuard } from './usuarios/guards/auth.guard';
import { RoleGuard } from './usuarios/guards/role.guard';
import { TokenInterceptor } from './usuarios/interceptors/token.interceptor';
import { AuthInterceptor } from './usuarios/interceptors/auth.interceptor';


import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';




import { BooksComponent } from './books/books.component';
import { BooksService } from './books/books.service';

import { CartComponent } from './cart/cart.component';
import { CartService } from './cart/cart.service';

import {MatCardModule} from '@angular/material/card';
import { Component } from '@angular/Core';

registerLocaleData(localeES, 'es');

const routes: Routes = [
  { path: '', redirectTo: '/books', pathMatch: 'full' },

  { path: 'books', component: BooksComponent },
  { path: 'cart', component: CartComponent },  


  { path: 'login', component: LoginComponent },

];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
 
    CartComponent,
    LoginComponent,

    BooksComponent,
    
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    [RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload'
    })],
    BrowserAnimationsModule, MatDatepickerModule, MatMomentDateModule,
    ReactiveFormsModule, MatAutocompleteModule, MatInputModule, MatFormFieldModule,
    MatCardModule 
  ], exports: [RouterModule],
  providers: [BooksService,CartService,
    { provide: LOCALE_ID, useValue: 'es' },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
