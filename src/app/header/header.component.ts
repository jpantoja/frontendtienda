import { Component, OnInit } from '@angular/core';
import { AuthService } from '../usuarios/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { SharedService } from '../Services/shared.service';
import { BooksService } from '../books/books.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent  implements OnInit{
  title: string = 'Tienda En Linea Harry Potter'


  constructor(private booksService: BooksService,private sharedService:SharedService,public authService: AuthService, private router: Router) { }

  ngOnInit()
  {

 

  }
  logout(): void {
    let username = this.authService.usuario.username;
    this.sharedService.updateCartCount(0);
    this.authService.logout();
    swal.fire('Logout', `Hola ${username}, has cerrado sesión con éxito!`, 'success');
    this.router.navigate(['/login']);
  }
}
