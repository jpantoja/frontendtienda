import { Injectable } from '@angular/core';
import { Book } from '../cart/models/book';

import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Router } from '@angular/router';

import { URL_BACKEND } from '../config/config';

import { ShoppingCart } from '../cart/models/shoppingCart';


@Injectable()
export class BooksService {
  private urlEndPoint: string = URL_BACKEND + '/api';

  constructor(private http: HttpClient, private router: Router) { }


  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.urlEndPoint +'/books');
  }

  addbookToCart(shoppingCart: ShoppingCart) {
    
    localStorage.setItem("shoppingCart", JSON.stringify(shoppingCart));
  }


  getbookFromCart() {
    //return localStorage.getItem("product");
    return JSON.parse(localStorage.getItem('shoppingCart'));
  }
  removeAllbookFromCart() {
    return localStorage.removeItem("shoppingCart");
  }



}
