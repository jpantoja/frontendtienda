import { Component, OnInit } from '@angular/core';
import { Book } from '../cart/models/book';
import { BooksService } from './books.service';
//import { ModalService } from './detalle/modal.service';
import swal from 'sweetalert2';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../usuarios/auth.service';

import { URL_BACKEND } from '../config/config';
import { SharedService } from '../Services/shared.service';

import { ShoppingCart } from '../cart/models/shoppingCart';
import { ItemsShoppingCart } from '../cart/models/itemsShoppingCart';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html'
})
export class BooksComponent implements OnInit {
  books: Book[];
  //shoppingCart: Book[];
  bookItemCount: number = 0;
  quantity: number = 0;

  //paginador: any;
  // bookSeleccionado: Book;
  urlBackend: string = URL_BACKEND;

  shoppingCart: ShoppingCart = new ShoppingCart();
  ob3: ShoppingCart = new ShoppingCart();
  stock: number = 0;


  constructor(private booksService: BooksService,
    private sharedService: SharedService,

    public authService: AuthService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {


    this.booksService.getBooks().subscribe(books => this.books = books);



    this.sharedService.updateCartCount(this.booksService.getbookFromCart.length);
  


    /* this.activatedRoute.paramMap.subscribe(params => {
       let page: number = +params.get('page');
 
       if (!page) {
         page = 0;
       }
       this.booksService.getBooks().subscribe(books => this.books = books);
     });
 */
    /*this.modalService.notificarUpload.subscribe(cliente => {
      this.books = this.books.map(clienteOriginal => {
        if (cliente.id == clienteOriginal.id) {
          clienteOriginal.foto = cliente.foto;
        }
        return clienteOriginal;
      })
    })*/
  }


  //this.factura.items = this.factura.items.filter((item: ItemFactura) => id !== item.producto.id);


  actualizarCantidad(book: Book, event: any): void {
    let cant: number = event.target.value as number;   
     

    book.quantity = cant;



  }


  OnAddCart(book: Book): void {
  

    this.stock = book.stock;








    if (Number(book.quantity) > Number(book.stock)) {
      swal.fire('Advertencia: ', `Solo hay disponible  ${book.stock} unidades.`, 'warning');
    } else if (book.quantity <= 0 || book.quantity === undefined) {
      swal.fire('Advertencia: ', 'Debe seleccionar la cantidad de libros', 'warning');
    } else if (Number(book.quantity) <= Number(book.stock) && Number(book.quantity) > 0) {






      this.shoppingCart = this.booksService.getbookFromCart();

      if (this.shoppingCart == null) {




        
        this.shoppingCart = new ShoppingCart();



        let nuevoItem = new ItemsShoppingCart();
        nuevoItem.book = book;
        nuevoItem.quantity = book.quantity;
     
        this.shoppingCart.itemsShoppingCart.push(nuevoItem);

    


        this.booksService.addbookToCart(this.shoppingCart);
        this.sharedService.updateCartCount(this.shoppingCart.itemsShoppingCart.length);


        swal.fire({
   
        icon: 'success',
        title: 'Se ha agregado el libro al carrito de compras',
        showConfirmButton: false,
        timer: 1500}
        );


      }
      else {



        let tempProduct = this.shoppingCart.itemsShoppingCart.find(p => p.book.id == book.id);
        if (tempProduct == null) {

          let nuevoItem = new ItemsShoppingCart();
          nuevoItem.book = book;
          nuevoItem.quantity = book.quantity;
          this.shoppingCart.itemsShoppingCart.push(nuevoItem);

   

          this.booksService.addbookToCart(this.shoppingCart);

         
    
          this.sharedService.updateCartCount(this.shoppingCart.itemsShoppingCart.length);
          swal.fire({
            
            icon: 'success',
            title: 'Se ha agregado el libro al carrito de compras',
            showConfirmButton: false,
            timer: 1500}
            );
    
        }
        else {
          swal.fire('Advertencia: ', 'El libro ya se encuentra en el carrito', 'warning');
        }

      }
  
      this.bookItemCount = this.shoppingCart.itemsShoppingCart.length;
  
      this.sharedService.updateCartCount(this.bookItemCount);

    



  





   

     

    }




  }
  



}















